package conf

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

type Configuration struct {
	Listen struct {
		Ip     string `json:"ip"`
		Port   uint   `json:"port"`
		Scheme string `json:"scheme"`
	} `json:"listen"`
	Logs struct {
		Log      string `json:"log"`
		Output   bool   `json:"output"`
		Remote   string `json:"remote"`
		Interval int    `json:"interval"`
	} `json:"logs"`
	Db struct {
		Type     string `json:"type"`
		Host     string `json:"host"`
		User     string `json:"user"`
		Password string `json:"password"`
		Dbname   string `json:"dbname"`
		Sslmode  string `json:"sslmode"`
		Port     int    `json:"port"`
	} `json:"db"`
	Redis struct {
		Host     string `json:"host"`
		Port     int    `json:"port"`
		Pool     int    `json:"pool"`
		Password string `json:"password"`
		Ttl      int    `json:"ttl"`
	} `json:"redis"`
	ListLimit int `json:"list_limit"`
	Templates struct {
		Dir  string `json:"dir"`
		Mode int    `json:"mode"`
	} `json:"templates"`
	Tokens struct {
		Access  uint `json:"access"`
		Refresh uint `json:"refresh"`
	} `json:"tokens"`
	SecretKey string                 `json:"secret_key"`
	Services  map[string]interface{} `json:"services"`
	Robot     struct {
		User     string `json:"user"`
		Password string `json:"password"`
		Host     string `json:"host"`
		Mail     struct {
			Server   string `json:"server"`
			Port     int    `json:"port"`
			User     string `json:"user"`
			Password string `json:"password"`
		} `json:"mail"`
		Recipients []string `json:"recipients"`
		Templates  string   `json:"templates"`
	} `json:"robot"`
	Machine struct {
		Hostname string `json:"hostname"`
	} `json:"machine"`
	Kassa struct {
		APIEndpoint string `json:"api_endpoint"`
		ShopId      int    `json:"shop_id"`
		Key         string `json:"key"`
		Test        bool   `json:"test"`
	} `json:"kassa"`
	SiteMap struct {
		Dir string `json:"dir"`
	} `json:"sitemap"`
}

func Read(fileName string) (conf *Configuration) {
	bytes, err := ioutil.ReadFile(fileName)
	if err != nil {
		panic(err)
		return nil
	}
	err = json.Unmarshal(bytes, &conf)
	if err != nil {
		panic(err)
		return nil
	}
	return conf
}

func (o *Configuration) SetEnv() {
	//Redis
	if k, ok := os.LookupEnv("REDIS_HOST"); ok {
		o.Redis.Host = k
	}
	if k, ok := os.LookupEnv("REDIS_PORT"); ok {
		k, err := strconv.Atoi(k)
		if err != nil {
			log.Println(err)
		} else {
			o.Redis.Port = k
		}
	}
	if k, ok := os.LookupEnv("REDIS_PASSWORD"); ok {
		o.Redis.Password = k
	}

	//Db
	if k, ok := os.LookupEnv("DB_TYPE"); ok {
		o.Db.Type = k
	}
	if k, ok := os.LookupEnv("DB_HOST"); ok {
		o.Db.Host = k
	}
	if k, ok := os.LookupEnv("DB_PORT"); ok {
		k, err := strconv.Atoi(k)
		if err != nil {
			log.Println(err)
		} else {
			o.Db.Port = k
		}
	}
	if k, ok := os.LookupEnv("DB_USER"); ok {
		o.Db.User = k
	}
	if k, ok := os.LookupEnv("DB_PASSWORD"); ok {
		o.Db.Password = k
	}

	//SecretKey
	if k, ok := os.LookupEnv("SECRET_KEY"); ok {
		o.SecretKey = k
	}

	//Robot
	if k, ok := os.LookupEnv("ROBOT_USER"); ok {
		o.Robot.User = k
	}
	if k, ok := os.LookupEnv("ROBOT_PASSWORD"); ok {
		o.Robot.Password = k
	}
	if k, ok := os.LookupEnv("ROBOT_MAIL_USER"); ok {
		o.Robot.Mail.User = k
	}
	if k, ok := os.LookupEnv("ROBOT_MAIL_PASSWORD"); ok {
		o.Robot.Mail.Password = k
	}

	//Kassa
	if k, ok := os.LookupEnv("KASSA_KEY"); ok {
		o.Kassa.Key = k
	}
}
